package com.example.abhinavpruthi.androidproject;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class FullImageActivity extends AppCompatActivity {

    ViewPager viewPager;
    CustomSwipeAdapter adapter;


    ExifInterface exif;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);

        viewPager=(ViewPager)findViewById(R.id.view_pager);
        Intent i = getIntent();
        ArrayList<File> list =(ArrayList<File>) i.getSerializableExtra("list");
        int pos = i.getIntExtra("img",0);


        try {
            exif = new ExifInterface(list.get(pos).getPath());
            Log.d("File ka path",list.get(pos).getPath());
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("position:",pos+"");
        adapter=new CustomSwipeAdapter(getApplicationContext(),list,pos);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(pos);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail,menu);
        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.photo_details)
        {
showDetails();
        }
        return true;
    }
    void showDetails()
    {
            try
            {
                exif = new ExifInterface(adapter.file.getPath());
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }


        String latitute = "";
        String longitude="";
         latitute =  exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
         longitude=  exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);

        Float latitude= 0F  ;
if(latitute!=null)
               latitude = convertToDegree(latitute);
        Float longi = 0F;
if(longitude!=null)
        longi= convertToDegree(longitude);
        String imgDate_Time = exif.getAttribute(ExifInterface.TAG_DATETIME);
        Toast.makeText(getApplicationContext(),latitute,Toast.LENGTH_LONG).show();
        new MaterialDialog.Builder(this)
                .title("Photo Details")
                .content("Latitude "+latitude+"\nLongitude"+longi+"\n Date Time "+imgDate_Time)
                .positiveText("OK")

                .show();

    }

    private Float convertToDegree(String stringDMS){
        Float result = null;
        String[] DMS = stringDMS.split(",", 3);

        String[] stringD = DMS[0].split("/", 2);
        Double D0 = new Double(stringD[0]);
        Double D1 = new Double(stringD[1]);
        Double FloatD = D0/D1;

        String[] stringM = DMS[1].split("/", 2);
        Double M0 = new Double(stringM[0]);
        Double M1 = new Double(stringM[1]);
        Double FloatM = M0/M1;

        String[] stringS = DMS[2].split("/", 2);
        Double S0 = new Double(stringS[0]);
        Double S1 = new Double(stringS[1]);
        Double FloatS = S0/S1;

        result = new Float(FloatD + (FloatM/60) + (FloatS/3600));

        return result;


    }
}
