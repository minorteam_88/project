package com.example.abhinavpruthi.androidproject;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Created by ABHINAV PRUTHI on 9/20/2016.
 */
public class ImageAdapter extends BaseAdapter {
    private Context context;
    public Integer[] images ={
        R.drawable.button_round_green,R.drawable.button_round_red,
        R.drawable.cartoon_turtle_round_head, R.drawable.round_stone_tower
    };
   public ImageAdapter(Context c){
       context=c;
   }
    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int position) {
        return images[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView=new ImageView(context);
        imageView.setImageResource(images[position]);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView.setLayoutParams(new GridView.LayoutParams(240,240));
        return imageView;
    }
}
