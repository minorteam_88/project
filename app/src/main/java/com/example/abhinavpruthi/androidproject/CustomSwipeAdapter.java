package com.example.abhinavpruthi.androidproject;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by ABHINAV PRUTHI on 9/23/2016.
 */
public class CustomSwipeAdapter extends PagerAdapter
{
    File file;
    ArrayList<File> list;
    private Context ctx;
    private LayoutInflater layoutInflater;
    int position1;
    public CustomSwipeAdapter(Context ctx,ArrayList<File> list,int position)
    {
        this.list = list;
        this.ctx=ctx;
       this.position1 = position;
        Log.d("thisposition",""+this.position1);
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return  (view==(LinearLayout)o);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position1) {
        layoutInflater =(LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item_view = layoutInflater.inflate(R.layout.swipe_layout,container,false);
        ImageView imageView=(ImageView)item_view.findViewById(R.id.image_view);
        TextView textView =(TextView) item_view.findViewById(R.id.image_count);
        file = list.get(position1);
        imageView.setImageURI(Uri.parse(list.get(position1).toString()));
        Log.d("CustomPos",""+position1);
        textView.setText("Image :"+position1);
        container.addView(item_view);


        return item_view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
